# ProcessAMLWorkflow
Este projeto é responsável por ler e analisar pedidos vindos do DCS ou do **BankaMW** e invocar outros projetos que irão invocar APIs da Banka (Aplicação Core).

- - -

Este projeto comunica através de um tópico e de uma mensagem JSON. Por exemplo:

1.	Projeto A --> **Publica** no NATS Streaming o tópico "XPTO" juntamente com uma mensagem JSON;
2.	NATS Streaming --> Recebe a publicação da mensagem com o respetivo tópico e notifica todos os projetos que estejam a subscrever o mesmo;
3.	Projeto B --> Subscreve a mensagem notificada pelo NATS Streaming e efetua a lógica desejada.

**NOTA:** Podem haver 1 ou mais sub-projetos a subscrever e/ou publicarem o mesmo tópico.

A nomenclatura usada nos tópicos é a seguinte: "{ação}:{Request/Response}".
Por exemplo, no caso da criação de uma entidade particular é o seguinte: "LockAccount:Request".

## **Sub-Projetos**
•	[ChangeAccountDetails](https://bitbucket.org/bai-europa/changeaccountdetails/src/dev/) – Este é o projeto responsável por invocar as APIs relativas a clientes;

•	[ChangePartyDetails](https://bitbucket.org/bai-europa/changepartydetails/src/dev/) - Este é o projeto responsável por invocar APIs relativas a entidades;

### **Situação Real**
Neste exemplo, encontra-se uma situação real na criação de uma entidade na Banka:

1.	É feita a criação da Entidade na Banka;
2.	O **BankaMW** recebe a transação feita pela Banka;
	2.1. Deteta que tipo de operação foi efetuada (neste caso a criação de uma entidade, mas poderia ser uma alteração de documentos, morada, etc);
	2.2. É chamado o microsserviço interno de Parties; 
	2.3 Depois da resposta obtida, é feita alguma lógica e enviada o pedido para o NATS;
3.	O NATS notifica os projetos que estão a subscrever o tópico enviado;
4.	O DCS_REST_Async subcreve ao tópico enviado pelo **BankaMW**;
   4.1. É feito um mapeamento da mensagem JSON recebida para o objeto correto de pedido para o DCS;
   4.2. O pedido é enviado para o serviço REST do DCS;
   4.3. Caso a resposta tenha sido de sucesso, é verificado se é para desbloquear a Entidade;
      4.3.1. Se sim, é enviado para o NATS o pedido de desbloqueio que vai ser subscrito pelo **ProcessAMLWorkflow**;
5.	 O ProcessAMLWorkflow invoca o serviço rest do projeto **ChangePartyDetails** para efetuar a alteração da Entidade na Banka;
6.	O workflow termina quando as alterações foram todas feitas na Banka.

- - -