package pt.baieuropa.baieserv.repository;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.baieuropa.baieserv.repository.entities.MongoStructure;

import java.util.List;

@Repository
public interface MongoStructureRepository extends MongoRepository<MongoStructure, String> {
    List<MongoStructure> findByRequestId(String requestId);
}
