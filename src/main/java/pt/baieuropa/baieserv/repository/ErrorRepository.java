package pt.baieuropa.baieserv.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pt.baieuropa.baieserv.repository.entities.ErrorEntity;

@Repository
public interface ErrorRepository extends MongoRepository<ErrorEntity, String> {

}
