package pt.baieuropa.baieserv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pt.baieuropa.baieserv.repository.entities.EntitySOAPDto;

import java.util.List;

@Repository
public interface EntitySOAPRepository extends JpaRepository<EntitySOAPDto, String> {

    @Query(nativeQuery = true)
    List<EntitySOAPDto> findByPartyId(@Param("partyId") String partyId);

}
