package pt.baieuropa.baieserv.repository.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Builder
public class EntitySOAPDto {

    @Column(name = "Entity_Name")
    private String entityName;
    @Column(name = "Doc_Id")
    private String docId;
    @Id
    @Column(name = "Nif")
    private String nif;
    @Column(name = "Birth_Dt")
    private String birthDt;

}
