package pt.baieuropa.baieserv.repository.entities;

import java.time.LocalDateTime;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("ErrorNats")
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorEntity {

  @Id
  private String id;
  private String topic;
  private String message;
  private String correlationId;
  private LocalDateTime dateTime;
}
