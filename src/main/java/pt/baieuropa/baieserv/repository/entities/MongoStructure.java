package pt.baieuropa.baieserv.repository.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MongoStructure {

    @Id
    private ObjectId id;
    private String requestId;
    private String serviceName;
    private String subject;
    private String message;
    private String status;
    private Boolean serviceResult;
    private String timestamp;

}
