package pt.baieuropa.baieserv.config.properties;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "mq.account")
public class AccountProperties {
  @NotBlank
  private String amlLockRequest;
  @NotBlank
  private String amlUnlockRequest;
  @NotBlank
  private String createRequest;
  @NotBlank
  private String createRiskRequest;
  @NotBlank
  private String lockRequest;
  @NotBlank
  private String unlockRequest;
  @NotBlank
  private String updateRequest;
  @NotBlank
  private String updateRiskRequest;
}
