package pt.baieuropa.baieserv.config.properties;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "mq.entity")
public class EntityProperties {
    @NotBlank
    private String enterpriseCreated;
    @NotBlank
    private String createParticularRequest;
    @NotBlank
    private String lockRequest;
    @NotBlank
    private String unlockRequest;
    @NotBlank
    private String updateRequest;
    @NotBlank
    private String enterpriseUpdated;
    @NotBlank
    private String particularUpdated;
    @NotBlank
    private String updateRiskRequest;

}
