/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.config;

import pt.baieuropa.baieserv.exceptions.ConfigurationException;

import java.util.HashMap;

/**
 * Class that will be the only point in the service where ALL the configurations of the Service will be read and stored.
 */
public class Properties {

    private HashMap<String, Object> propertiesMap;

    private String configurationFile_String;

    /**
     * Properties from file will mainly retrieve the properties of the database where the service stores is configurations
     */
    public boolean readPropertiesFromFile() {

        // TODO fill the Configuration File Path
        this.configurationFile_String = "";

        return false;
    }

    private boolean connectToConfigurationDatabase() {

        return false;
    }

    /**
     * All the configurations MUST be stored on the service configurations database
     */
    public boolean readPropertiesFromDataBase() throws ConfigurationException {
        if (!this.connectToConfigurationDatabase()) {
            throw new ConfigurationException("The connection to the configurations database was not successful. Please check the configuration file " + this.configurationFile_String);
        } else {
            return false;
        }
    }

    /**
     * Returns the property value for a given property name
     */
    public Object getPropertyByName() {

        return null;
    }


    public HashMap<String, Object> getPropertiesMap() {
        return propertiesMap;
    }

    public void setPropertiesMap(HashMap<String, Object> propertiesMap) {
        this.propertiesMap = propertiesMap;
    }
}
