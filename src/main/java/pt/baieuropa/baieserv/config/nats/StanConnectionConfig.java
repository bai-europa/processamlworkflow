package pt.baieuropa.baieserv.config.nats;

import java.io.IOException;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pt.baieuropa.nats.Stan;
import pt.baieuropa.nats.configurations.NatsProperties;

@Configuration
@RequiredArgsConstructor
public class StanConnectionConfig {

  private final NatsProperties natsProperties;

  @Bean
  public Stan connect() {
    try {
      final var stan = new Stan();
      stan.connectToNats(natsProperties);
      return stan;
    } catch (IOException | InterruptedException e) {
      throw new RuntimeException(e);
    }
  }
}