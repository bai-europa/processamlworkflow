package pt.baieuropa.baieserv.config.nats.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pt.baieuropa.baieserv.consumer.NATSRequestsManager;
import pt.baieuropa.baieserv.service.impl.AccountSOAPServiceImpl;
import pt.baieuropa.baieserv.service.impl.AmlSOAPServiceImpl;
import pt.baieuropa.baieserv.service.impl.EntitySOAPImpl;

@Configuration
public class Entity {

    @Bean("LockEntity:Request")
    public NATSRequestsManager lockEntityean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                             AccountSOAPServiceImpl accountSOAPServiceImpl,
                                             EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("LockEntity:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("UnlockEntity:Request")
    public NATSRequestsManager unlockEntityBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("UnlockEntity:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("UpdateEntityRisk:Request")
    public NATSRequestsManager updateEntityRiskBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                    AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                    EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("UpdateEntityRisk:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("UpdateFiltering:Request")
    public NATSRequestsManager updateFilteringBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                   AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                   EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("UpdateFiltering:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("UpdateEntity:Request")
    public NATSRequestsManager updateEntityBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("UpdateEntity:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

}
