package pt.baieuropa.baieserv.config.nats;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pt.baieuropa.nats.Runner;
import pt.baieuropa.nats.Stan;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;
import pt.baieuropa.nats.configurations.NatsProperties;
import pt.baieuropa.nats.service.FallbackNats;

@Slf4j
@Component
@RequiredArgsConstructor
public class SubscribeNats {
    private static final String SERVICE_NAME = "SERVICE";
    private final List<Runner<Void>> runnables;
    private final Stan stan;
    private final NatsProperties natsProperties;
    private final FallbackNats fallbackNats;

    @PostConstruct
    public void subscribeNats() {
        runnables.forEach(run ->
                {
                    try {
                        stan.subscribeToTopic(run.getTopico(), SERVICE_NAME, run, fallbackNats, natsProperties);
                    } catch (InterruptedException | TimeoutException | IOException e) {
                        log.warn("Unsubscribe the topic: {} because: {}", run.getTopico(),
                                e.getLocalizedMessage(), e);
                    }
                }
        );
    }
}