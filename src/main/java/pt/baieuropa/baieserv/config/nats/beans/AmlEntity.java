package pt.baieuropa.baieserv.config.nats.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pt.baieuropa.baieserv.consumer.NATSRequestsManager;
import pt.baieuropa.baieserv.service.impl.AccountSOAPServiceImpl;
import pt.baieuropa.baieserv.service.impl.AmlSOAPServiceImpl;
import pt.baieuropa.baieserv.service.impl.EntitySOAPImpl;

@Configuration
public class AmlEntity {

    @Bean("AML:RequestLockEntity")
    public NATSRequestsManager amlLockAccountBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                  AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                  EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("AML:RequestLockEntity", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("AML:RequestUnlockEntity")
    public NATSRequestsManager amlUnlockAccountBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                    AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                    EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("AML:RequestUnlockEntity", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }
}
