package pt.baieuropa.baieserv.config.nats.fallback;

import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;
import pt.baieuropa.baieserv.repository.ErrorRepository;
import pt.baieuropa.baieserv.repository.entities.ErrorEntity;
import pt.baieuropa.nats.service.FallbackNats;

@Service
@Slf4j
@RequiredArgsConstructor
public class FallbackNatsImpl implements FallbackNats {

  private final ErrorRepository errorRepository;

  @Override
  public void process(String topic, String content) {
    try {
      errorRepository.save(getEntity(topic, content));
    } catch (RuntimeException e) {
      log.error(e.getMessage(), e);
    }
  }

  private ErrorEntity getEntity(String topic, String content) {
    final var correlationId = MDC.get("correlation_id");
    return ErrorEntity.builder()
        .topic(topic)
        .message(content)
        .correlationId(correlationId)
        .dateTime(LocalDateTime.now()).build();
  }
}
