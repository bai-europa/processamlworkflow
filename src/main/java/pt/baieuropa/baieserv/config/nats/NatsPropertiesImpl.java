package pt.baieuropa.baieserv.config.nats;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import pt.baieuropa.nats.configurations.NatsProperties;

@Setter
@Configuration
@ConfigurationProperties(prefix = "nats")
public class NatsPropertiesImpl implements NatsProperties {

  private String host;
  private String port;
  private String clusterId;
  private String clientId;
  private int numberRetries;
  private Long delayBetweenRetries;

  @Override
  public String getHostname() {
    return host;
  }

  @Override
  public String getPort() {
    return port;
  }

  @Override
  public String getClusterId() {
    return clusterId;
  }

  @Override
  public String getClientId() {
    return clientId;
  }

  @Override
  public int getNumberRetries() {
    return numberRetries;
  }

  @Override
  public Long getDelayBetweenRetries() {
    return delayBetweenRetries;
  }
}
