package pt.baieuropa.baieserv.config.nats.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pt.baieuropa.baieserv.consumer.NATSRequestsManager;
import pt.baieuropa.baieserv.service.impl.AccountSOAPServiceImpl;
import pt.baieuropa.baieserv.service.impl.AmlSOAPServiceImpl;
import pt.baieuropa.baieserv.service.impl.EntitySOAPImpl;

@Configuration
public class Account {

    @Bean("LockAccount:Request")
    public NATSRequestsManager lockAccountBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                               AccountSOAPServiceImpl accountSOAPServiceImpl,
                                               EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("LockAccount:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("UnlockAccount:Request")
    public NATSRequestsManager unlockAccountBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                 AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                 EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("UnlockAccount:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("UpdateAccountRisk:Request")
    public NATSRequestsManager updateAccountRiskBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                     AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                     EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("UpdateAccountRisk:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("CreateAccountRisk:Request")
    public NATSRequestsManager createAccountRiskBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                     AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                     EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("CreateAccountRisk:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("CreateAccount:Request")
    public NATSRequestsManager createAccountBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                 AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                 EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("CreateAccount:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }

    @Bean("UpdateAccount:Request")
    public NATSRequestsManager updateAccountBean(AmlSOAPServiceImpl amlSOAPServiceImpl,
                                                 AccountSOAPServiceImpl accountSOAPServiceImpl,
                                                 EntitySOAPImpl entitySOAPImpl) {
        return new NATSRequestsManager("UpdateAccount:Request", amlSOAPServiceImpl, accountSOAPServiceImpl, entitySOAPImpl);
    }
}
