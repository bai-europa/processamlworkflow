package pt.baieuropa.baieserv.config.nats;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;
import pt.baieuropa.nats.Stan;
import pt.baieuropa.nats.configurations.NatsProperties;

@Component
@RequiredArgsConstructor
public class NatsHealthIndicator implements HealthIndicator {

  private final Stan stan;
  private final NatsProperties natsProperties;

  @Override
  public Health health() {
    if (checkServiceHealth()) {
      return Health.up().withDetail("Nats", "Available").build();
    }
    return Health.down().withDetail("Nats", "Unavailable").build();
  }

  private boolean checkServiceHealth() {
    try {
      stan.connectToNats(natsProperties);
    } catch (Exception e) {
      if (e.getMessage().contains("Unable to connect to NATS servers")) {
        return false;
      }
    }
    return true;
  }
}