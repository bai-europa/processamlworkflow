package pt.baieuropa.baieserv.config.nats;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pt.baieuropa.nats.Stan;
import pt.baieuropa.nats.configurations.NatsProperties;

@Slf4j
@Component
@EnableAsync
@RequiredArgsConstructor
public class NatsReconnect {

  private final NatsProperties natsProperties;
  private final Stan stan;

  @Async
  @Scheduled(fixedDelay = 900000)
  public void reconnect() {
    try {
      stan.connectToNats(natsProperties);
    } catch (Exception e) {
      final var message = "Unable to connect to NATS servers";
      if (e.getMessage().contains(message)) {
        log.error("NatsReconnect@reconnect - Output - {}", message);
        return;
      }
    }
    log.info("NatsReconnect@reconnect - Output - Nats is connected");
  }

}
