package pt.baieuropa.baieserv.config.logging;

import lib.baieuropa.aop.AspectBaie;
import lib.baieuropa.aop.config.AspectBean;
import lib.baieuropa.aop.config.AspectProperties;
import lib.baieuropa.aop.errors.handle.ErrorMessageHandle;
import lib.baieuropa.aop.logs.LoggingAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig extends AspectBean {
  @Bean
  public AspectBaie aspectBaie(LoggingAspect loggingAspect, ErrorMessageHandle errorMessageHandle,
      AspectProperties properties) {
    return new AspectBaie(loggingAspect, errorMessageHandle, properties);
  }
}