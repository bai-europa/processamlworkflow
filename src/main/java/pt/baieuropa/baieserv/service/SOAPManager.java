package pt.baieuropa.baieserv.service;


import javax.xml.soap.*;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import pt.baieuropa.baieserv.service.dto.soap.SoapDTO;

@Slf4j
@Service
public class SOAPManager {



    public void callSoapWebService(String soapEndpointUrl, String soapAction, String receivedBodyNamespace,
                                String receivedBodyNamespaceURI, String receivedElementNamespace,
                                String receivedXSDNamespaceURI, HashMap<String, Object> receivedMap) {
        try {
            log.debug("Creating SOAP Connection...");
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            log.debug("SOAP Connection Established.");

            // Set's global variables
            final var soapDTO = SoapDTO.builder().bodyNamespace(receivedBodyNamespace)
                .bodyNamespaceURI(receivedBodyNamespaceURI)
                .elementNamespace(receivedElementNamespace)
                .elementNamespaceURI(receivedXSDNamespaceURI)
                .action(soapAction)
                .map(receivedMap).build();

            // Send SOAP Message to SOAP Server
            try {
                SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapEndpointUrl + soapAction, soapDTO), soapEndpointUrl);
                //soapResponse.writeTo(System.out);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);
                log.info("SOAP Response: \n{}", baos.toString());
            } catch (Exception ex) {
                log.error("Exception while sending SOAP Message to SOAP Server: {}", ex.getMessage(), ex);
            }

            soapConnection.close();
            log.debug("SOAP Connection Closed.");
        } catch (Exception e) {
            log.error("Error occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!{}", e.getMessage(), e);

        }
    }

    public SOAPMessage createSOAPRequest(String soapAction, SoapDTO soapDTO) throws Exception {
        log.debug("Creating SOAP Request...");

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        try {
            Thread.sleep(500);
            createSoapEnvelope(soapMessage, soapDTO);

            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader("SOAPAction", soapAction);

            soapMessage.saveChanges();

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMessage.writeTo(out);
            String strMsg = new String(out.toByteArray());

            log.info("\nRequest Received: \n{}\n\n", strMsg);

        } catch (Exception ex) {
            log.error("Exception while Creating SOAP Request: \n{}", ex.getMessage());
        }

        log.debug("SOAP Request Created");
        return soapMessage;
    }

    public void createSoapEnvelope(SOAPMessage soapMessage, SoapDTO soapDTO) {
        log.debug("Creating SOAP envelope...");
        SOAPPart soapPart = soapMessage.getSOAPPart();

        try {
            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration(soapDTO.getBodyNamespace(), soapDTO.getBodyNamespaceURI());
            envelope.addNamespaceDeclaration(soapDTO.getElementNamespace(),soapDTO.getElementNamespaceURI());

            // SOAP Body
            SOAPBody soapBody = envelope.getBody();
            SOAPElement soapBodyElem = soapBody.addChildElement(soapDTO.getAction(),soapDTO.getBodyNamespace());
            SOAPElement dados = soapBodyElem.addChildElement("dados", soapDTO.getBodyNamespace());
            for(String key : soapDTO.getMap().keySet()) {
                if(soapDTO.getMap().get(key) != null)
                    dados.addChildElement(key, soapDTO.getElementNamespace()).addTextNode(soapDTO.getMap().get(key).toString());
            }

//            QName soapAction = new QName(myNamespaceURIACT, action, myNamespaceACT);
//            SOAPElement soapBodyAction = soapBody.addChildElement(soapAction);
//
//            QName soapDados = new QName(myNamespaceURIACT, "dados", myNamespaceACT);
//            SOAPElement soapBodyDados = soapBodyAction.addChildElement(soapDados);
//
//            for(String key : map.keySet()) {
//                if(map.get(key) != null)
//                    soapBodyAction.addChildElement(key, myNamespaceURIXSD).addTextNode(map.get(key).toString());
//            }

            // action
//            QName childName = new QName(myNamespaceURIACT,action,"act");
//            SOAPBodyElement soapBodyElem = soapBody.addBodyElement(childName);

            // dados
//            QName childName2 = new QName(myNamespaceURIACT,"dados","act");
//            SOAPBodyElement soapBodyElem2 = soapBody.addBodyElement(childName2);

//            soapBodyElem.addChildElement(soapBodyElem2);

//            QName temp = new QName(myNamespaceURIXSD,"","xsd");
//            SOAPBodyElement tempBody = soapBody.addBodyElement(temp);
//            for(String key : map.keySet()) {
//                if(map.get(key) != null)
//                    tempBody.addChildElement(key, myNamespaceURIXSD).addTextNode(map.get(key).toString());
//
//                    soapBodyElem2.addChildElement(tempBody);
//            }

        } catch (Exception ex) {
            log.error("Exception while creting SOAP envelope: \n{}", ex.getMessage());
        }
        log.debug("SOAP Envelope Created.");
    }
}
