package pt.baieuropa.baieserv.service;

import org.json.JSONObject;

public interface ActiveMQService {

  void publish(String subject, JSONObject content);

  void publish(String topic, String content);
}
