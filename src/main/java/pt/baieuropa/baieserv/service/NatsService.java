package pt.baieuropa.baieserv.service;


import pt.baieuropa.baieserv.service.dto.atualizarRiscoCliente.ActualizacaoNivelRiscoCliente;
import pt.baieuropa.baieserv.service.dto.bloqueioConta.CriarBloqueioConta;
import pt.baieuropa.baieserv.service.dto.bloqueioEntidade.CriarBloqueioEntidade;
import pt.baieuropa.baieserv.service.dto.desativarConta.DesactivarBloqueioConta;

public interface NatsService {

    void publishMessage(String topic, CriarBloqueioConta criarBloqueioConta);
    void publishMessage(String topic, DesactivarBloqueioConta desactivarBloqueioConta);
    void publishMessage(String topic, ActualizacaoNivelRiscoCliente actualizacaoNivelRiscoCliente);
    void publishMessage(String topic, CriarBloqueioEntidade criarBloqueioEntidade);
    void publishMessage(String topic, String json);
    boolean isTopicNats(String topic);
}
