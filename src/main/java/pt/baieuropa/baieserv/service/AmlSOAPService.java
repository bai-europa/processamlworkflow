package pt.baieuropa.baieserv.service;

public interface AmlSOAPService {

    int lockEntity(String content);
    int unlockEntity(String content);
}
