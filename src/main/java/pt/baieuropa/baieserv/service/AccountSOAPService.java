package pt.baieuropa.baieserv.service;

public interface AccountSOAPService {

    int lockAccountByParty(String json);
    int updateAccountByParty(String json);
    int lockAccount(String json);
    int unlockAccount(String json);
    int updateAccountRisk(String json);
}
