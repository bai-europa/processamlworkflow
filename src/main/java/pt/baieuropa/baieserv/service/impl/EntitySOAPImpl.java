package pt.baieuropa.baieserv.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pt.baieuropa.baieserv.models.entity.LockPartyInput;
import pt.baieuropa.baieserv.models.entity.RaisinInput;
import pt.baieuropa.baieserv.models.entity.UnlockPartyInput;
import pt.baieuropa.baieserv.repository.EntitySOAPRepository;
import pt.baieuropa.baieserv.repository.MongoStructureRepository;
import pt.baieuropa.baieserv.repository.entities.EntitySOAPDto;
import pt.baieuropa.baieserv.service.*;
import pt.baieuropa.baieserv.service.dto.atualizaEstadoFiltering.AtualizaEstadoFiltering;
import pt.baieuropa.baieserv.service.dto.atualizarNivelRiscoEntidade.ActualizacaoNivelRiscoEntidade;
import pt.baieuropa.baieserv.service.dto.bloqueioEntidade.CriarBloqueioEntidade;
import pt.baieuropa.baieserv.service.dto.bloqueioEntidade.ObjectInputCriarBloqueioEntidade;
import pt.baieuropa.baieserv.service.dto.desativarEntidade.DesactivarBloqueioEntidade;
import pt.baieuropa.baieserv.service.dto.desativarEntidade.ObjectInputDesactivarBloqueioEntidade;
import pt.baieuropa.baieserv.utils.BuildMongoStructure;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EntitySOAPImpl implements EntitySOAPService {

    private static final Logger log = LoggerFactory.getLogger(EntitySOAPImpl.class);
    private final SOAPManager soapManager = new SOAPManager();
    private final SOAPManagerRaisin soapManagerRaisin;
    private final ObjectMapper mapper;
    private final NatsService natsService;
    private final ActiveMQService activeMQService;
    private final MongoStructureRepository mongoStructureRepository;
    private final EntitySOAPRepository entityRepository;
    @Value("${client.lockEntityURL}")
    public String lockEntityURL;
    @Value("${client.unlockEntityURL}")
    public String unlockEntityURL;
    @Value("${client.trytecomMW}")
    public String trytecomMWOnEntity;
    @Value("${client.unlockEntityRaisin}")
    public String unlockEntityRaisin;

    private static final MediaType MediaTypeJSON = MediaType
            .parse("application/json; charset=utf-8");

    //BAIE-------BANKAMW------
    @Override
    public int lockEntityByParty(String json) {
        log.debug("Locking Entity by Party...");
        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating JSON object...");
            JSONObject jsonObject = new JSONObject(json);
            String entityNumber = jsonObject.getString("partyId");
            log.debug("JSON object created SUCCESSFULLY.");

            log.debug("Creating Main Object...");
            CriarBloqueioEntidade criarBloqueioEntidade = new CriarBloqueioEntidade();
            ObjectInputCriarBloqueioEntidade input = new ObjectInputCriarBloqueioEntidade();
            input.setAutoriz("999");
            input.setCodigoNotaBloqueio(0);
            input.setMotivoBloqueio("");
            input.setNumeroEntidade(Integer.parseInt(entityNumber));
            input.setObservacoes("BLOQUEIO AML DCS");
            input.setSiglaParaMensagemAlerta("AML");
            input.setTipoBloqueio("A");
            input.setUtilizador("TYIUSER");
            input.setValidarOuAplicar("A");
            criarBloqueioEntidade.setDados(input);
            log.debug("Main object created SUCCESSFULLY");

            log.debug("Generating HashMap with the main object...");
            HashMap<String, Object> map = mapper.convertValue(criarBloqueioEntidade.getDados(), new TypeReference<HashMap<String, Object>>() {});
            log.debug("HashMap generated SUCCESSFULLY\n" + map);

            log.info("Calling SOAP Service [ CriarBloqueioEntidade ]");
            soapManager.callSoapWebService(
                    trytecomMWOnEntity + "CriarBloqueioEntidade",
                    "criarBloqueioEntidade",
                    "cri",
                    "http://criarBloqueioEntidade.entidadesclientes",
                    "xsd",
                    "http://criarBloqueioEntidade.entidadesclientes/xsd",
                    map);
            log.info("SOAP Service [ CriarBloqueioEntidade ] Executed.");

            log.debug("Entity Locked by Party SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Lock Entity by Party: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int updateEntityByParty(String json) {
        log.debug("Updating Entity by Party...");
        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating JSON object...");
            JSONObject jsonObject = new JSONObject(json);
            String entityNumber = jsonObject.getString("partyId");
            log.debug("JSON object created SUCCESSFULLY.");

            log.debug("Creating Main Object...");
            ObjectInputCriarBloqueioEntidade input = new ObjectInputCriarBloqueioEntidade();
            input.setAutoriz("999");
            input.setCodigoNotaBloqueio(0);
            input.setMotivoBloqueio("");
            input.setNumeroEntidade(Integer.parseInt(entityNumber));
            input.setObservacoes("BLOQUEIO AML DCS");
            input.setSiglaParaMensagemAlerta("AML");
            input.setTipoBloqueio("A");
            input.setUtilizador("TYIUSER");
            input.setValidarOuAplicar("A");
            CriarBloqueioEntidade criarBloqueioEntidade = new CriarBloqueioEntidade();
            criarBloqueioEntidade.setDados(input);
            log.debug("Main object created SUCCESSFULLY");

            log.debug("Generating HashMap with the main object...");
            HashMap<String, Object> map = mapper.convertValue(criarBloqueioEntidade.getDados(), new TypeReference<HashMap<String, Object>>() {});
            log.debug("HashMap generated SUCCESSFULLY\n" + map);

            log.info("Calling SOAP Service [ CriarBloqueioEntidade ]");
            soapManager.callSoapWebService(
                    trytecomMWOnEntity + "CriarBloqueioEntidade",
                    "criarBloqueioEntidade",
                    "act",
                    "http://criarBloqueioEntidade.entidadesclientes",
                    "xsd",
                    "http://criarBloqueioEntidade.entidadesclientes/xsd",
                    map);
            log.info("SOAP Service [ CriarBloqueioEntidade ] Executed.");

            log.debug("Entity Updated by Party SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Update Entity by Party: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int unlockEntityByParty(String json) {
        log.debug("Unlocking Entity by Party...");
        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating JSON object...");
            JSONObject jsonObject = new JSONObject(json);
            String partyID = jsonObject.getString("partyId");
            log.debug("JSON object created SUCCESSFULLY.");

            log.debug("Creating Main Object...");
            DesactivarBloqueioEntidade criarBloqueioEntidade = new DesactivarBloqueioEntidade();
            ObjectInputDesactivarBloqueioEntidade input = new ObjectInputDesactivarBloqueioEntidade();
            input.setAutoriz("999");
            input.setCodigoNotaBloqueio(0);
            input.setNumeroEntidade(Integer.parseInt(partyID));
            input.setObservacoes("BLOQUEIO AML DCS");
            input.setTipoBloqueio("A");
            input.setValidarOuAplicar("A");
            criarBloqueioEntidade.setDados(input);
            log.debug("Main object created SUCCESSFULLY");

            log.debug("Generating HashMap with the main object...");
            HashMap<String, Object> map = mapper.convertValue(criarBloqueioEntidade.getDados(), new TypeReference<HashMap<String, Object>>() {});
            log.debug("HashMap generated SUCCESSFULLY\n" + map);

            log.info("Calling SOAP Service [ DesactivarBloqueioEntidade ]");
            soapManager.callSoapWebService(
                    trytecomMWOnEntity + "DesactivarBloqueioEntidade",
                    "desactivarBloqueioEntidade",
                    "des",
                    "http://desactivarBloqueioEntidade.entidadesclientes",
                    "xsd",
                    "http://desactivarBloqueioEntidade.entidadesclientes/xsd",
                    map);
            log.info("SOAP Service [ DesactivarBloqueioEntidade ] Executed.");

            log.debug("Entity Unlocked by Party SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Unlock Entity by Party: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int updateEntity(String json){
        log.debug("Updating Entity...");
        try {

            log.debug("Creating JSON object...");
            JSONObject jsonObject = new JSONObject(json);
            String entityNumber = jsonObject.getString("partyId");
            log.debug("JSON object created SUCCESSFULLY.");

            //BAIE-----QUERY Á BANKA PARA VERIFICAR SE ENTIDADE É RAISIN----

            LinkedList<Object> queryParams = new LinkedList<>();
            queryParams.add(entityNumber);

            log.debug("Running query");

            List<EntitySOAPDto> entities = this.entityRepository.findByPartyId(entityNumber);
            entities.forEach(e -> {

                log.debug("Creating Main Object...");
                RaisinInput raisinInput = new RaisinInput();
                raisinInput.setEntityName(e.getEntityName().trim());
                raisinInput.setDocId(e.getDocId().trim());
                raisinInput.setNif(e.getNif().trim());
                raisinInput.setBirthDt(e.getBirthDt().trim());
                raisinInput.setEntityNr(entityNumber);
                log.debug("Main object created SUCCESSFULLY");

                HashMap<String, Object> map = mapper.convertValue(raisinInput, new TypeReference<HashMap<String, Object>>() {});
                log.debug("HashMap generated SUCCESSFULLY\n" + map);

                log.info("Calling SOAP Service [ Update Entidade Raisin ]");
                soapManagerRaisin.callSoapWebService(
                        unlockEntityRaisin + "RaisinEntityUpdate",
                        "getEntityId",
                        "upd",
                        "http://update.entity.raisin.bai.pt/",
                        "",
                        "", map);
                log.debug("Entity Updated SUCCESSFULLY");

            });
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Update Entity: \n" + ex.getLocalizedMessage());
            return 0;
        }
    }

    @Override
    //BAIE-------DCS---------
    public int lockEntity(String json) {
        log.debug("Locking Entity...");

        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating JSON object...");
            CriarBloqueioEntidade obj = mapper.readValue(json, CriarBloqueioEntidade.class);
            log.debug("JSON object created SUCCESSFULLY.");

            // BAIE - Verifica se o requestId existe no mongo e foi tratado.
            log.debug("Checking if the request has already been processed...");
            log.debug("Request checked successfully.");

            log.debug("Creating Main Object...");
            //BAIE transformar json na estrutura que o ChangeAccountDetails vai usar
            LockPartyInput lockPartyInput = new LockPartyInput();
            lockPartyInput.setPartyId(obj.getDados().getNumeroEntidade().toString());
            lockPartyInput.setBlockType(obj.getDados().getTipoBloqueio());
            lockPartyInput.setBlockCode(obj.getDados().getCodigoNotaBloqueio().toString());
            lockPartyInput.setAuthorization(obj.getDados().getAutoriz());
            lockPartyInput.setValidateOrApply(obj.getDados().getValidarOuAplicar());
            lockPartyInput.setUser(obj.getDados().getUtilizador());
            log.debug("Main object created SUCCESSFULLY");

            log.debug("Converting Transaction Object to JSON...");
            Gson gson = new Gson();
            String transactionJSON = gson.toJson(lockPartyInput);
            log.debug("Object Converted.");

            final var topic = "LockEntity:Request";
            publishTopic(topic, transactionJSON);

            Response response = null;
            OkHttpClient client = new OkHttpClient();

            log.debug("Calling WebService " + lockEntityURL + transactionJSON);
            Request request = new Request.Builder()
                    .url(lockEntityURL)
                    .post(RequestBody.create(transactionJSON, MediaTypeJSON))
                    .build();

            response = client.newCall(request).execute();
            log.debug("Response Received: \n" + response.body());

            log.debug("Saving request success response on database...");
            mongoStructureRepository.save(BuildMongoStructure.buildMongoStructure(obj.getDados().getRequestId(),
                    topic, json));

            log.debug("Entity Locked SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Entity Lock: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int unlockEntity(String json) {
        log.debug("Unlocking Entity...");
        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating JSON object...");
            DesactivarBloqueioEntidade obj = mapper.readValue(json, DesactivarBloqueioEntidade.class);
            log.debug("JSON object created SUCCESSFULLY.");

            // BAIE - Verifica se o requestId existe no mongo e foi tratado.
            log.debug("Checking if the request has already been processed...");
            log.debug("Request checked successfully.");

            log.debug("Creating Main Object...");
            UnlockPartyInput unlockPartyInput = new UnlockPartyInput();
            unlockPartyInput.setPartyId(obj.getDados().getNumeroEntidade().toString());
            unlockPartyInput.setBlockType(obj.getDados().getTipoBloqueio());
            unlockPartyInput.setBlockCode(obj.getDados().getCodigoNotaBloqueio().toString());
            unlockPartyInput.setAuthorization(obj.getDados().getAutoriz());
            unlockPartyInput.setValidateOrApply(obj.getDados().getValidarOuAplicar());
            unlockPartyInput.setUser(obj.getDados().getUtilizador());
            log.debug("Main object created SUCCESSFULLY");

            log.debug("Converting Transaction Object to JSON...");
            Gson gson = new Gson();
            String transactionJSON = gson.toJson(unlockPartyInput);
            log.debug("Object Converted.");

            final var topic = "UnlockEntity:Request";
            publishTopic(topic, transactionJSON);

            OkHttpClient client = new OkHttpClient();

            log.debug("Calling WebService " + unlockEntityURL + transactionJSON);
            Request request = new Request.Builder()
                    .url(unlockEntityURL)
                    .post(RequestBody.create(transactionJSON, MediaTypeJSON))
                    .build();

            Response response = client.newCall(request).execute();

            //BAIE-----QUERY Á BANKA PARA VERIFICAR SE ENTIDADE É RAISIN----

            LinkedList<Object> queryParams = new LinkedList<>();
            queryParams.add(obj.getDados().getNumeroEntidade().toString());

            log.debug("Running query");
            List<EntitySOAPDto> entities = this.entityRepository.findByPartyId(obj.getDados().getNumeroEntidade().toString());

            entities.forEach(e -> {

                log.debug("Creating Main Object...");
                //BAIE INVOCAR ENDPOINT RAISIN PARA PARA ATUALIZAR ENTIDADE(para desbloqueada)
                RaisinInput raisinInput = new RaisinInput();
                // Não pegar do objeto e não da env
                raisinInput.setEntityName(e.getEntityName());
                raisinInput.setDocId(e.getDocId());
                raisinInput.setNif(e.getNif());
                raisinInput.setBirthDt(e.getBirthDt());
                raisinInput.setEntityNr(obj.getDados().getNumeroEntidade().toString());
                log.debug("Main object created SUCCESSFULLY");

                log.debug("Generating HashMap with the main object...");
                HashMap<String, Object> map = mapper.convertValue(raisinInput, new TypeReference<HashMap<String, Object>>() {
                });
                log.debug("HashMap generated SUCCESSFULLY\n" + map);

                log.info("Calling SOAP Service [ Update Entidade Raisin ]");
                soapManagerRaisin.callSoapWebService(
                        unlockEntityRaisin + "RaisinEntityUpdate",
                        "getEntityId",
                        "upd",
                        "http://update.entity.raisin.bai.pt/",
                        "",
                        "", map);
                log.info("SOAP Service [ Update Entidade Raisin ] Executed.");

            });

            log.debug("Entity Unlocked SUCCESSFULLY");

            log.debug("Saving request success response on database...");
            mongoStructureRepository.save(BuildMongoStructure.buildMongoStructure(obj.getDados().getRequestId(),
                    topic, json));
            log.debug("Request success response saved.");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Unlock Entity: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int updateEntityRisk(String json) {
        log.debug("Updating Account by Party...");
        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating Main Object...");
            ActualizacaoNivelRiscoEntidade obj = mapper.readValue(json, ActualizacaoNivelRiscoEntidade.class);
            log.debug("Main object created SUCCESSFULLY");

            // BAIE - Verifica se o requestId existe no mongo e foi tratado.
            log.debug("Checking if the request has already been processed...");
            log.debug("Request checked successfully.");

            HashMap<String, Object> map = mapper.convertValue(obj.getDados(), new TypeReference<HashMap<String, Object>>() {
            });
            log.debug("HashMap generated SUCCESSFULLY\n" + map);

            log.info("Calling SOAP Service [ ActualizacaoNivelRiscoEntidade ]");
            soapManager.callSoapWebService(
                    trytecomMWOnEntity + "ActualizacaoNivelRiscoEntidade",
                    "actualizacaoNivelRiscoEntidade",
                    "act",
                    "http://actualizacaonivelriscoentidade.consultas",
                    "xsd",
                    "http://actualizacaonivelriscoentidade.consultas/xsd",
                    map);
            log.info("SOAP Service [ ActualizacaoNivelRiscoEntidade ] Executed.");

            log.debug("Saving request success response on database...");
            mongoStructureRepository.save(BuildMongoStructure.buildMongoStructure(obj.getDados().getRequestId(),
                    "UpdateEntityRisk:Request", json));

            log.debug("Entity Risk Updated SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Update Entity Risk: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int updateFiltering(String json) {
        log.debug("Updating Account by Party...");
        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating Main object...");
            AtualizaEstadoFiltering obj = mapper.readValue(json, AtualizaEstadoFiltering.class);
            log.debug("Main object created SUCCESSFULLY");

            // BAIE - Verifica se o requestId existe no mongo e foi tratado.
            log.debug("Checking if the request has already been processed...");
            log.debug("Request checked successfully.");

            log.debug("Generating HashMap with the main object...");
            HashMap<String, Object> map = mapper.convertValue(obj.getDados(), new TypeReference<HashMap<String, Object>>() {
            });
            log.debug("HashMap generated SUCCESSFULLY\n" + map);

            log.info("Calling SOAP Service [ AtualizaEstadoFiltering ]");
            soapManager.callSoapWebService(
                    trytecomMWOnEntity + "AtualizaEstadoFiltering",
                    "atualizaEstadoFiltering",
                    "atu",
                    "http://atualizaestadofiltering.consultas",
                    "xsd",
                    "http://atualizaestadofiltering.consultas/xsd",
                    map);
            log.info("SOAP Service [ AtualizaEstadoFiltering ] Executed.");

            log.debug("Saving request success response on database...");
            mongoStructureRepository.save(BuildMongoStructure.buildMongoStructure(obj.getDados().getRequestId(),
                    "UpdateFiltering:Request", json));

            log.debug("Filtering Updated SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Update Filtering: \n" + ex.getMessage());
            return 0;
        }
    }

    private void publishTopic(String topic, String content) {
        if(natsService.isTopicNats(topic)){
            natsService.publishMessage(topic, content);
            return;
        }
        activeMQService.publish(topic, content);
    }

}
