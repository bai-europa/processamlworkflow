package pt.baieuropa.baieserv.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import pt.baieuropa.baieserv.config.properties.AccountProperties;
import pt.baieuropa.baieserv.models.account.ClientRiskInput;
import pt.baieuropa.baieserv.models.account.LockAccountInput;
import pt.baieuropa.baieserv.models.account.UnlockAccountInput;
import pt.baieuropa.baieserv.repository.entities.MongoStructure;
import pt.baieuropa.baieserv.repository.MongoStructureRepository;
import pt.baieuropa.baieserv.service.AccountSOAPService;
import pt.baieuropa.baieserv.service.ActiveMQService;
import pt.baieuropa.baieserv.service.NatsService;
import pt.baieuropa.baieserv.service.SOAPManager;
import pt.baieuropa.baieserv.service.dto.atualizarRiscoCliente.ActualizacaoNivelRiscoCliente;
import pt.baieuropa.baieserv.service.dto.bloqueioConta.CriarBloqueioConta;
import pt.baieuropa.baieserv.service.dto.bloqueioConta.ObjectInputCriarBloqueioConta;
import pt.baieuropa.baieserv.service.dto.desativarConta.DesactivarBloqueioConta;
import pt.baieuropa.baieserv.utils.BuildMongoStructure;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class AccountSOAPServiceImpl implements AccountSOAPService {

    private static final Logger log = LoggerFactory.getLogger(AccountSOAPServiceImpl.class);

    private final SOAPManager soapManager;
    private final ObjectMapper mapper;
    private final NatsService natsService;
    private final ActiveMQService activeMQService;
    private final AccountProperties accountProperties;
    private final MongoStructureRepository mongoStructureRepository;
    @Value("${client.lockAccountURL}")
    public String lockAccountURL;
    @Value("${client.unlockAccountURL}")
    public String unlockAccountURL;
    @Value("${client.updateAccountRiskURL}")
    public String updateAccountRiskURL;
    @Value("${client.createAccountRiskURL}")
    public String createAccountRiskURL;
    @Value("${client.trytecomMW}")
    public String trytecomMWOnAccounts;

    private static final MediaType MediaTypeJSON = MediaType
            .parse("application/json; charset=utf-8");


    @Override
    public int lockAccountByParty(String json) {
        log.debug("Locking Account by Party...");
        try {
            log.debug("Creating JSON object...");
            JSONObject jsonObject = new JSONObject(json);
            JSONObject AccountObject = jsonObject.getJSONObject("Account");

            JSONObject acctInfo = AccountObject.getJSONObject("acctInfo");
            JSONObject acctIdent = acctInfo.getJSONObject("acctIdent");
            String accountNumber = acctIdent.getString("acctIdentValue");
            String clientNumber = acctInfo.getString("ownership");
            log.debug("JSON object created SUCCESSFULLY.");

            log.debug("Creating Main Object...");
            CriarBloqueioConta criarBloqueioConta = new CriarBloqueioConta();
            ObjectInputCriarBloqueioConta input = new ObjectInputCriarBloqueioConta();
            input.setAutoriz("999");
            input.setClasseBloqueada("");
            input.setCodigoNotaBloqueio(0);
            input.setMotivoBloqueio("");
            input.setNumeroCliente(Integer.parseInt(clientNumber));//TODO alterar
            input.setNumeroConta(accountNumber);
            input.setObservacoes("BLOQUEIO AML DCS");
            input.setSiglaParaMensagemAlerta("AML");
            input.setTipoBloqueio("T");
            input.setUtilizador("TYIUSER");
            input.setValidarOuAplicar("A");
            criarBloqueioConta.setDados(input);
            log.debug("Main object created SUCCESSFULLY");
            log.debug("Generating HashMap with the main object...");
            HashMap<String, Object> map = mapper.convertValue(criarBloqueioConta.getDados(), new TypeReference<HashMap<String, Object>>() {});
            log.debug("HashMap generated SUCCESSFULLY\n" + map);

            log.debug("Account Number: " + accountNumber + "\n" +
                      "Client Number: " + clientNumber);

            log.info("Calling SOAP Service [ CriarBloqueioConta ]");
            soapManager.callSoapWebService(
                    trytecomMWOnAccounts + "CriarBloqueioConta",
                    "criarBloqueioConta",
                    "cri",
                    "http://criarBloqueioConta.entidadesclientes",
                    "xsd",
                    "http://criarBloqueioConta.entidadesclientes/xsd",
                    map);
            log.info("SOAP Service [ CriarBloqueioConta ] Executed.");

            log.debug("Account Locked by Party SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Account Lock by Party: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int updateAccountByParty(String json) {
        log.debug("Updating Account by Party...");
        try {
            log.debug("Creating JSON object...");
            JSONObject jsonObject = new JSONObject(json);
            JSONObject AccountObject = jsonObject.getJSONObject("Account");

            JSONObject acctInfo = AccountObject.getJSONObject("acctInfo");
            JSONObject acctIdent = acctInfo.getJSONObject("acctIdent");
            String accountNumber = acctIdent.getString("acctIdentValue");
            String clientNumber = acctInfo.getString("ownership");
            log.debug("JSON object created SUCCESSFULLY.");

            log.debug("Creating Main Object...");
            CriarBloqueioConta criarBloqueioConta = new CriarBloqueioConta();
            ObjectInputCriarBloqueioConta input = new ObjectInputCriarBloqueioConta();
            input.setAutoriz("999");
            input.setClasseBloqueada("");
            input.setCodigoNotaBloqueio(0);
            input.setMotivoBloqueio("");
            input.setNumeroCliente(Integer.parseInt(clientNumber));//TODO alterar
            input.setNumeroConta(accountNumber);
            input.setObservacoes("BLOQUEIO AML DCS");
            input.setSiglaParaMensagemAlerta("AML");
            input.setTipoBloqueio("T");
            input.setUtilizador("TYIUSER");
            input.setValidarOuAplicar("A");
            criarBloqueioConta.setDados(input);
            log.debug("Main object created SUCCESSFULLY");

            log.debug("Generating HashMap with the main object...");
            HashMap<String, Object> map = mapper.convertValue(criarBloqueioConta.getDados(), new TypeReference<HashMap<String, Object>>() {});
            log.debug("HashMap generated SUCCESSFULLY\n" + map);

            log.debug("Account Number: " + accountNumber + "\n" +
                    "Client Number: " + clientNumber);

            log.info("Calling SOAP Service [ CriarBloqueioConta ]");
            soapManager.callSoapWebService(
                    trytecomMWOnAccounts + "CriarBloqueioConta",
                    "criarBloqueioConta",
                    "act",
                    "http://criarBloqueioConta.entidadesclientes",
                    "xsd",
                    "http://criarBloqueioConta.entidadesclientes/xsd",
                    map);
            log.info("SOAP Service [ CriarBloqueioConta ] Executed.");

            log.debug("Account Updated by Party SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Update Account by Party: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int lockAccount(String json) {
        log.debug("Locking Account...");
        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating JSON object...");
            CriarBloqueioConta obj = mapper.readValue(json, CriarBloqueioConta.class);
            log.debug("JSON object created SUCCESSFULLY.");

            // BAIE - Verifica se o requestId existe no mongo e foi tratado.
            log.debug("Checking if the request has already been processed...");
            List<MongoStructure> result = mongoStructureRepository.findByRequestId(obj.getDados().getRequestId());

            if(Objects.nonNull(result) && !result.isEmpty()) {
                log.debug("The request [" + obj.getDados().getRequestId() + "] has already been processed.");
                return 0;
            } else {

                log.debug("Creating Main Object...");
                //BAIE transformar json na estrutura que o ChangeAccountDetails vai usar
                LockAccountInput lockAccountInput = new LockAccountInput();
                lockAccountInput.setUser(obj.getDados().getUtilizador());
                lockAccountInput.setaccountNumber(obj.getDados().getNumeroConta());
                lockAccountInput.setblockType(obj.getDados().getTipoBloqueio());
                lockAccountInput.setblockCode(obj.getDados().getCodigoNotaBloqueio().toString());
                lockAccountInput.setalertMessage(obj.getDados().getSiglaParaMensagemAlerta());
                lockAccountInput.setauthorization(obj.getDados().getAutoriz());
                lockAccountInput.setvalidateOrApply(obj.getDados().getValidarOuAplicar());
                log.debug("Main object created SUCCESSFULLY");

                log.debug("Converting Transaction Object to JSON...");
                Gson gson = new Gson();
                String transactionJSON = gson.toJson(lockAccountInput);
                log.debug("Object Converted.");
                final var topic = "LockAccount:Request";
                publishTopic(topic, transactionJSON);

                Response response = null;
                OkHttpClient client = new OkHttpClient();

                log.debug("Calling WebService " + lockAccountURL + transactionJSON);
                Request request = new Request.Builder()
                        .url(lockAccountURL)
                        .post(RequestBody.create(transactionJSON, MediaTypeJSON))
                        .build();

                response = client.newCall(request).execute();
                log.debug("Response Received: \n" + response.body());

                log.debug("Saving request success response on database...");
                mongoStructureRepository.save(BuildMongoStructure.buildMongoStructure(obj.getDados().getRequestId(),
                    topic, json));

                log.debug("Request success response saved.");

                log.debug("Account Locked SUCCESSFULLY");
                return 1;
            }
        } catch (Exception ex) {
            log.error("Some error occurred while running Lock Account: \n" + ex.getMessage());
            return 0;
        }
    }

    private void publishTopic(String topic, String content) {
        if(natsService.isTopicNats(topic)){
            natsService.publishMessage(topic, content);
            return;
        }
        activeMQService.publish(topic, content);
    }

    @Override
    public int unlockAccount(String json) {
        log.debug("Unlocking Account...");
        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating JSON object...");
            DesactivarBloqueioConta obj = mapper.readValue(json, DesactivarBloqueioConta.class);
            log.debug("JSON object created SUCCESSFULLY.");

            // BAIE - Verifica se o requestId existe no mongo e foi tratado.
            log.debug("Checking if the request has already been processed...");
            log.debug("Request checked successfully.");

            //BAIE transformar json na estrutura que o ChangeAccountDetails vai usar
            UnlockAccountInput unlockAccountInput = new UnlockAccountInput();
            unlockAccountInput.setUser(obj.getDados().getUtilizador());
            unlockAccountInput.setAccountNumber(obj.getDados().getNumeroConta());
            unlockAccountInput.setClientNumber(obj.getDados().getNumeroCliente().toString());
            unlockAccountInput.setBlockType(obj.getDados().getTipoBloqueio());
            unlockAccountInput.setUnlockCode(obj.getDados().getCodigoNotaDesbloqueio().toString());
            unlockAccountInput.setAlertMessage(obj.getDados().getSiglaParaMensagemAlerta());
            unlockAccountInput.setAuthorization(obj.getDados().getAutoriz());
            unlockAccountInput.setValidateOrApply(obj.getDados().getValidarOuAplicar());
            log.debug("Main object created SUCCESSFULLY");

            Gson gson = new Gson();
            String transactionJSON = gson.toJson(unlockAccountInput);
            Response response = null;
            OkHttpClient client = new OkHttpClient();

            log.debug("Calling WebService " + unlockAccountURL + transactionJSON);
            Request request = new Request.Builder()
                    .url(unlockAccountURL)
                    .post(RequestBody.create(transactionJSON, MediaTypeJSON))
                    .build();

            response = client.newCall(request).execute();
            log.debug("Response Received: \n" + response.body());

            final var topic = "UnlockAccount:Request";
            publishTopic(topic, transactionJSON);

            log.debug("Saving request success response on database...");
            mongoStructureRepository.save(BuildMongoStructure.buildMongoStructure(obj.getDados().getRequestId(),
                    topic, json));

            log.debug("Account Unlocked SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Unlock Account: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int updateAccountRisk(String json) {
        log.debug("Updating Account Risk...");
        try {
            log.debug("JSON Received: \n" + json);

            log.debug("Creating JSON object...");
            ActualizacaoNivelRiscoCliente obj = mapper.readValue(json, ActualizacaoNivelRiscoCliente.class);
            log.debug("JSON object created SUCCESSFULLY.");

            // BAIE - Verifica se o requestId existe no mongo e foi tratado.
            log.debug("Checking if the request has already been processed...");
            log.debug("Request checked successfully.");

            log.debug("Creating Main Object...");
            //BAIE transformar json na estrutura que o ChangeAccountDetails vai usar
            ClientRiskInput clientRiskInput = new ClientRiskInput();
            clientRiskInput.setUser(obj.getDados().getUtilizador());
            clientRiskInput.setClientNumber(obj.getDados().getNumeroCliente().toString());
            clientRiskInput.setAction(obj.getDados().getAccao());
            clientRiskInput.setAuthorization(obj.getDados().getAutorizacao());
            clientRiskInput.setFieldCode(obj.getDados().getCodigoCampo());
            clientRiskInput.setBranchNumber(obj.getDados().getNumeroBalcao().toString());
            clientRiskInput.setFieldValue(obj.getDados().getValorCampo());
            log.debug("Main object created SUCCESSFULLY");

            log.debug("Converting Transaction Object to JSON...");
            Gson gson = new Gson();
            String transactionJSON = gson.toJson(clientRiskInput);
            log.debug("Object Converted.");

            String accao = obj.getDados().getAccao();
            String finalUrl = "";
            String finalTopic = "";
            if (accao.equals("Criar") || accao.equals("A") || accao.equals("Add") || accao.equals("ADD")) {
                finalUrl = createAccountRiskURL;
                final var topic = "LockAccount:Request";
                finalTopic = topic;
                publishTopic(topic, transactionJSON);
            }
            if (accao.equals("ALTERAR") || accao.equals("C") || accao.equals("Change") || accao.equals("CHANGE")) {
                finalUrl = updateAccountRiskURL;
                final var topic = "UpdateAccountRisk:Request";
                finalTopic = topic;
                publishTopic(topic, transactionJSON);
            }

            Response response = null;
            OkHttpClient client = new OkHttpClient();

            log.debug("Calling WebService " + finalUrl + transactionJSON);
            Request request = new Request.Builder()
                    .url(finalUrl)
                    .post(RequestBody.create(transactionJSON, MediaTypeJSON))
                    .build();
            response = client.newCall(request).execute();
            log.debug("Response Received: \n" + response.body());

            log.debug("Saving request success response on database...");
            mongoStructureRepository.save(BuildMongoStructure.buildMongoStructure(obj.getDados().getRequestId(),
                    finalTopic, json));

            log.debug("Account Risk Update SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Update Account Risk: \n" + ex.getMessage());
            return 0;
        }
    }
}
