package pt.baieuropa.baieserv.service.impl;

import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import pt.baieuropa.baieserv.service.ActiveMQService;

@Service
@RequiredArgsConstructor
public class ActiveMqServiceImpl implements ActiveMQService {

  private final JmsTemplate jmsTemplate;

  @Override
  public void publish(String topic, JSONObject content) {
    jmsTemplate.convertAndSend(topic, content);
  }

  @Override
  public void publish(String topic, String content) {
    jmsTemplate.convertAndSend(topic, content);
  }
}
