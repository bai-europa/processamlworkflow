package pt.baieuropa.baieserv.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pt.baieuropa.baieserv.service.AmlSOAPService;
import pt.baieuropa.baieserv.service.SOAPManager;
import pt.baieuropa.baieserv.service.dto.bloqueioEntidade.CriarBloqueioEntidade;
import pt.baieuropa.baieserv.service.dto.bloqueioEntidade.ObjectInputCriarBloqueioEntidade;
import pt.baieuropa.baieserv.service.dto.desativarEntidade.DesactivarBloqueioEntidade;
import pt.baieuropa.baieserv.service.dto.desativarEntidade.ObjectInputDesactivarBloqueioEntidade;

import java.util.HashMap;

@RequiredArgsConstructor
@Service
public class AmlSOAPServiceImpl implements AmlSOAPService {

    private static final Logger log = LoggerFactory.getLogger(AmlSOAPServiceImpl.class);
    private final SOAPManager soapManager;
    private final ObjectMapper mapper;
    @Value("${client.trytecomMW}")
    private String trytecomMWOnAml;

    @Override
    public int lockEntity(String content) {
        log.debug("Locking Entity...");
        try {
            log.debug("JSON Received: \n" + content);

            log.debug("Creating JSON object...");
            JSONObject jsonObject = new JSONObject(content);
            String entityNumber = jsonObject.getString("partyAcctRelId");
            log.debug("JSON object created SUCCESSFULLY.");

            log.debug("Creating Main Object...");
            CriarBloqueioEntidade criarBloqueioEntidade = new CriarBloqueioEntidade();
            ObjectInputCriarBloqueioEntidade input = new ObjectInputCriarBloqueioEntidade();
            input.setAutoriz("999");
            input.setCodigoNotaBloqueio(0);
            input.setMotivoBloqueio("");
            input.setNumeroEntidade(Integer.parseInt(entityNumber));
            input.setObservacoes("BLOQUEIO AML DCS");
            input.setSiglaParaMensagemAlerta("AML");
            input.setTipoBloqueio("A");
            input.setUtilizador("TYIUSER");
            input.setValidarOuAplicar("A");
            criarBloqueioEntidade.setDados(input);
            log.debug("Main object created SUCCESSFULLY");

            log.debug("Generating HashMap with the main object...");
            HashMap<String, Object> map = mapper.convertValue(criarBloqueioEntidade.getDados(), new TypeReference<HashMap<String, Object>>() {});
            log.debug("HashMap generated SUCCESSFULLY\n" + map);

            log.info("Calling SOAP Service [ CriarBloqueioEntidade ]");
            soapManager.callSoapWebService(
                    trytecomMWOnAml + "CriarBloqueioEntidade",
                    "criarBloqueioEntidade",
                    "cri",
                    "http://criarBloqueioEntidade.entidadesclientes",
                    "xsd",
                    "http://criarBloqueioEntidade.entidadesclientes/xsd",
                    map);
            log.info("SOAP Service [ CriarBloqueioEntidade ] Executed.");

            //BAIE Publicar no nats o numero de entidade
            log.debug("Publish Entity Number on NATS with topic LockEntityAML:Response...");
            log.debug("Message published SUCCESSFULLY");

            log.debug("Entity Locked SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Entity Lock: \n" + ex.getMessage());
            return 0;
        }
    }

    @Override
    public int unlockEntity(String content) {
        log.debug("Locking Entity...");
        try {
            log.debug("JSON Received: \n" + content);

            log.debug("Creating JSON object...");
            JSONObject jsonObject = new JSONObject(content);
            String entityNumber = jsonObject.getString("partyAcctRelId");
            log.debug("JSON object created SUCCESSFULLY.");

            log.debug("Creating Main Object...");
            DesactivarBloqueioEntidade desactivarBloqueioEntidade = new DesactivarBloqueioEntidade();
            ObjectInputDesactivarBloqueioEntidade input = new ObjectInputDesactivarBloqueioEntidade();
            input.setAutoriz("999");
            input.setCodigoNotaBloqueio(0);
            input.setNumeroEntidade(Integer.parseInt(entityNumber));
            input.setObservacoes("BLOQUEIO AML DCS");
            //input.setSiglaParaMensagemAlerta(content.getString("#MENTHENT"));//TODO preencher
            input.setTipoBloqueio("A");
            //input.setUtilizador(content.getString("#MENTUSERC"));TODO preencher
            input.setValidarOuAplicar("A");
            desactivarBloqueioEntidade.setDados(input);
            log.debug("Main object created SUCCESSFULLY");

            log.debug("Generating HashMap with the main object...");
            HashMap<String, Object> map = mapper.convertValue(desactivarBloqueioEntidade.getDados(), new TypeReference<HashMap<String, Object>>() {});
            log.debug("HashMap generated SUCCESSFULLY\n" + map);

            log.info("Calling SOAP Service [ DesactivarBloqueioEntidade ]");
            soapManager.callSoapWebService(
                    trytecomMWOnAml + "DesactivarBloqueioEntidade",
                    "desactivarBloqueioEntidade",
                    "des",
                    "http://desactivarBloqueioEntidade.entidadesclientes",
                    "xsd",
                    "http://desactivarBloqueioEntidade.entidadesclientes/xsd",
                    map);
            log.info("SOAP Service [ DesactivarBloqueioEntidade ] Executed.");

            log.debug("Entity Unlocked SUCCESSFULLY");
            return 1;
        } catch (Exception ex) {
            log.error("Some error occurred while running Unlock Entity: \n" + ex.getMessage());
            return 0;
        }
    }
}
