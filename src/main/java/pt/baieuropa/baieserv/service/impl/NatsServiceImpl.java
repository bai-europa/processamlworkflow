package pt.baieuropa.baieserv.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import pt.baieuropa.baieserv.exceptions.NatsRequestException;
import pt.baieuropa.baieserv.service.NatsService;
import pt.baieuropa.baieserv.service.dto.atualizarRiscoCliente.ActualizacaoNivelRiscoCliente;
import pt.baieuropa.baieserv.service.dto.bloqueioConta.CriarBloqueioConta;
import pt.baieuropa.baieserv.service.dto.bloqueioEntidade.CriarBloqueioEntidade;
import pt.baieuropa.baieserv.service.dto.desativarConta.DesactivarBloqueioConta;
import pt.baieuropa.nats.Stan;
import pt.baieuropa.nats.configurations.NatsProperties;
import pt.baieuropa.nats.service.FallbackNats;

@Service
@RequiredArgsConstructor
public class NatsServiceImpl implements NatsService {

    private final Stan stan;
    private final ObjectMapper mapper;
    private final NatsProperties natsProperties;
    private final FallbackNats fallbackNats;

    @SneakyThrows
    @Override
    public void publishMessage(String topic, CriarBloqueioConta criarBloqueioConta) {
        try {
            publish(topic, mapper.writeValueAsString(criarBloqueioConta));
        } catch (JsonProcessingException e) {
            throw new NatsRequestException(e.getMessage());
        }
    }
    @SneakyThrows
    @Override
    public void publishMessage(String topic, DesactivarBloqueioConta desactivarBloqueioConta) {
        try {
            publish(topic, mapper.writeValueAsString(desactivarBloqueioConta));
        } catch (JsonProcessingException e) {
            throw new NatsRequestException(e.getMessage());
        }
    }

    @SneakyThrows
    @Override
    public void publishMessage(String topic, ActualizacaoNivelRiscoCliente actualizacaoNivelRiscoCliente) {
        try {
            publish(topic, mapper.writeValueAsString(actualizacaoNivelRiscoCliente));
        } catch (JsonProcessingException e) {
            throw new NatsRequestException(e.getMessage());
        }

    }

    @SneakyThrows
    public void publishMessage(String topic, CriarBloqueioEntidade criarBloqueioEntidade) {
        try {
            publish(topic, mapper.writeValueAsString(criarBloqueioEntidade));
        } catch (JsonProcessingException e) {
            throw new NatsRequestException(e.getMessage());
        }
    }

    @SneakyThrows
    @Override
    public void publishMessage(String topic, String json) {
        publish(topic, json);
    }

    @Override
    public boolean isTopicNats(String topic) {
        return topic.contains(":");
    }

    private void publish(String topic, String message) {
        stan.insertRequestOnTopic(topic, message, fallbackNats, natsProperties);
    }
}