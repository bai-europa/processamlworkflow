
package pt.baieuropa.baieserv.service.dto.bloqueioEntidade;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectOutputCriarBloqueioEntidade", namespace = "http://criarBloqueioEntidade.entidadesclientes/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
@Data
public class ObjectOutputCriarBloqueioEntidade {

    @XmlElement(namespace = "http://criarBloqueioEntidade.entidadesclientes/xsd")
    private Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://criarBloqueioEntidade.entidadesclientes/xsd", required = false)
    private String mensagemErro;
}
