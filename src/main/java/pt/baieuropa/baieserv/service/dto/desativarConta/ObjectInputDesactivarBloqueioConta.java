
package pt.baieuropa.baieserv.service.dto.desativarConta;


import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ObjectInputDesactivarBloqueioConta {

    private String requestId;
    
    @NotNull(message = "The property [autoriz] can't be NULL.")
    private String autoriz;
    @NotNull(message = "The property [codigoNotaDesbloqueio] can't be NULL.")
    private Integer codigoNotaDesbloqueio;
    @NotNull(message = "The property [numeroCliente] can't be NULL.")
    private Integer numeroCliente;
    @NotNull(message = "The property [numeroConta] can't be NULL.")
    private String numeroConta;
    @NotNull(message = "The property [observacoesDesativacao] can't be NULL.")
    private String observacoesDesativacao;
    @NotNull(message = "The property [siglaParaMensagemAlerta] can't be NULL.")
    private String siglaParaMensagemAlerta;
    @NotNull(message = "The property [tipoBloqueio] can't be NULL.")
    private String tipoBloqueio;
    @NotNull(message = "The property [utilizador] can't be NULL.")
    private String utilizador;
    @NotNull(message = "The property [validarOuAplicar] can't be NULL.")
    private String validarOuAplicar;

}
