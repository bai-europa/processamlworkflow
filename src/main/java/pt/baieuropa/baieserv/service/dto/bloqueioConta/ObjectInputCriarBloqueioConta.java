
package pt.baieuropa.baieserv.service.dto.bloqueioConta;


import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ObjectInputCriarBloqueioConta {

    private String requestId;

    @NotNull(message = "The property [autoriz] can't be NULL.")
    private String autoriz;
    @NotNull(message = "The property [classeBloqueada] can't be NULL.")
    private String classeBloqueada;
    @NotNull(message = "The property [codigoNotaBloqueio] can't be NULL.")
    private Integer codigoNotaBloqueio;
    @NotNull(message = "The property [motivoBloqueio] can't be NULL.")
    private String motivoBloqueio;
    @NotNull(message = "The property [numeroCliente] can't be NULL.")
    private Integer numeroCliente;
    @NotNull(message = "The property [numeroConta] can't be NULL.")
    private String numeroConta;
    @NotNull(message = "The property [observacoes] can't be NULL.")
    private String observacoes;
    @NotNull(message = "The property [siglaParaMensagemAlerta] can't be NULL.")
    private String siglaParaMensagemAlerta;
    @NotNull(message = "The property [tipoBloqueio] can't be NULL.")
    private String tipoBloqueio;
    @NotNull(message = "The property [utilizador] can't be NULL.")
    private String utilizador;
    @NotNull(message ="The property [validarOuAplicar] can't be NULL.")
    private String validarOuAplicar;

}
