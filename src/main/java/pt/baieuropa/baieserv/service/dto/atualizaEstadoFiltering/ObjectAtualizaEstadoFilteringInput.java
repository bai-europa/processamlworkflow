package pt.baieuropa.baieserv.service.dto.atualizaEstadoFiltering;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ObjectAtualizaEstadoFilteringInput {

    private String requestId;

    @NotNull(message = "The property [codigoElemento] can't be NULL.")
    private String codigoElemento;
    @NotNull(message = "The property [novoValor] can't be NULL.")
    private String novoValor;
    @NotNull(message = "The property [numeroEntidade] can't be NULL.")
    private Integer numeroEntidade;
    @NotNull(message = "The property [utilizador] can't be NULL.")
    private String utilizador;
}
