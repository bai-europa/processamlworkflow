
package pt.baieuropa.baieserv.service.dto.atualizaEstadoFiltering;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "atualizaEstadoFilteringResponse", namespace = "http://atualizaestadofiltering.consultas")
@Data
public class AtualizaEstadoFilteringResponse {

    @XmlElementRef(name = "return", namespace = "http://atualizaestadofiltering.consultas", required = false)
    private ObjectAtualizaEstadoFilteringOutput _return;

}
