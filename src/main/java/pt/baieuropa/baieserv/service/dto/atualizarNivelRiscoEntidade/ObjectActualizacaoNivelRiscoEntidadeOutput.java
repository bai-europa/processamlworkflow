
package pt.baieuropa.baieserv.service.dto.atualizarNivelRiscoEntidade;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectActualizacaoNivelRiscoEntidadeOutput", namespace = "http://actualizacaonivelriscoentidade.consultas/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
@Data
public class ObjectActualizacaoNivelRiscoEntidadeOutput {

    @XmlElement(namespace = "http://actualizacaonivelriscoentidade.consultas/xsd")
    private Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://actualizacaonivelriscoentidade.consultas/xsd", required = false)
    private String mensagemErro;
}
