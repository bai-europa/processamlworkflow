package pt.baieuropa.baieserv.service.dto.soap;

import java.util.HashMap;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SoapDTO {
  private String action;
  private String bodyNamespace;
  private String bodyNamespaceURI;
  private String elementNamespace;
  private String elementNamespaceURI;
  private HashMap<String, Object> map;
}
