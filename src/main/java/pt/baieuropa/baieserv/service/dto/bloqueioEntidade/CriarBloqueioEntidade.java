
package pt.baieuropa.baieserv.service.dto.bloqueioEntidade;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "criarBloqueioEntidade", namespace = "http://criarBloqueioEntidade.entidadesclientes")
@Data
public class CriarBloqueioEntidade {

    @XmlElementRef(name = "dados", namespace = "http://criarBloqueioEntidade.entidadesclientes", required = false)
    private ObjectInputCriarBloqueioEntidade dados;

}
