
package pt.baieuropa.baieserv.service.dto.bloqueioConta;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "criarBloqueioConta", namespace = "http://criarBloqueioConta.entidadesclientes")
@Data
public class CriarBloqueioConta {

    private ObjectInputCriarBloqueioConta dados;

}
