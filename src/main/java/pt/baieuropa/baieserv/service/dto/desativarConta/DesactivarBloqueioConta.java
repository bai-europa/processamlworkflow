
package pt.baieuropa.baieserv.service.dto.desativarConta;

import lombok.Data;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "desactivarBloqueioConta", namespace = "http://desactivarBloqueioConta.entidadesclientes")
@Data
public class DesactivarBloqueioConta {

    @XmlElementRef(name = "dados", namespace = "http://desactivarBloqueioConta.entidadesclientes", required = false)
    private ObjectInputDesactivarBloqueioConta dados;

}
