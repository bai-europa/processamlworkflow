
package pt.baieuropa.baieserv.service.dto.atualizarRiscoCliente;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "actualizacaoNivelRiscoCliente", namespace = "http://actualizacaonivelriscocliente.consultas")
@JsonPropertyOrder({
        "dados"
})
@Data
public class ActualizacaoNivelRiscoCliente {

    @XmlElementRef(name = "dados", namespace = "http://actualizacaonivelriscocliente.consultas", required = false)
    @JsonProperty("dados")
    private ObjectActualizacaoNivelRiscoClienteInput dados;


}
