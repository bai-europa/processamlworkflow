
package pt.baieuropa.baieserv.service.dto.atualizarNivelRiscoEntidade;

import lombok.Data;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "actualizacaoNivelRiscoEntidade", namespace = "http://actualizacaonivelriscoentidade.consultas")
@Data
public class ActualizacaoNivelRiscoEntidade {

    @XmlElementRef(name = "dados", namespace = "http://actualizacaonivelriscoentidade.consultas", required = false)
    private ObjectActualizacaoNivelRiscoEntidadeInput dados;

}
