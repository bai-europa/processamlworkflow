
package pt.baieuropa.baieserv.service.dto.desativarConta;

import lombok.Data;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectOutputDesactivarBloqueioConta", namespace = "http://desactivarBloqueioConta.entidadesclientes/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
@Data
public class ObjectOutputDesactivarBloqueioConta {

    @XmlElement(namespace = "http://desactivarBloqueioConta.entidadesclientes/xsd")
    private Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://desactivarBloqueioConta.entidadesclientes/xsd", required = false)
    private String mensagemErro;

}
