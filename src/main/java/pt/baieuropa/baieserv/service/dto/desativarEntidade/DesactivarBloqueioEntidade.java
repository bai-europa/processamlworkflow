
package pt.baieuropa.baieserv.service.dto.desativarEntidade;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "desactivarBloqueioEntidade", namespace = "http://desactivarBloqueioEntidade.entidadesclientes")
@Data
public class DesactivarBloqueioEntidade {

    @XmlElementRef(name = "dados", namespace = "http://desactivarBloqueioEntidade.entidadesclientes", required = false)
    private ObjectInputDesactivarBloqueioEntidade dados;

}
