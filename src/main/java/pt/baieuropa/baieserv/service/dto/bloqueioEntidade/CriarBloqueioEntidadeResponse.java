
package pt.baieuropa.baieserv.service.dto.bloqueioEntidade;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "criarBloqueioEntidadeResponse", namespace = "http://criarBloqueioEntidade.entidadesclientes")
@Data
public class CriarBloqueioEntidadeResponse {

    @XmlElementRef(name = "return", namespace = "http://criarBloqueioEntidade.entidadesclientes", required = false)
    private ObjectOutputCriarBloqueioEntidade _return;

}
