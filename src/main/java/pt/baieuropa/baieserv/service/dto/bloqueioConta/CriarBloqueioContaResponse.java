
package pt.baieuropa.baieserv.service.dto.bloqueioConta;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "criarBloqueioContaResponse", namespace = "http://criarBloqueioConta.entidadesclientes")
@Data
public class CriarBloqueioContaResponse {

    @XmlElementRef(name = "return", namespace = "http://criarBloqueioConta.entidadesclientes", required = false)
    private ObjectOutputCriarBloqueioConta _return;


}
