
package pt.baieuropa.baieserv.service.dto.atualizaEstadoFiltering;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectAtualizaEstadoFilteringOutput", namespace = "http://atualizaestadofiltering.consultas/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
@Data
public class ObjectAtualizaEstadoFilteringOutput {

    @XmlElement(namespace = "http://atualizaestadofiltering.consultas/xsd")
    private Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://atualizaestadofiltering.consultas/xsd", required = false)
    private String mensagemErro;

}
