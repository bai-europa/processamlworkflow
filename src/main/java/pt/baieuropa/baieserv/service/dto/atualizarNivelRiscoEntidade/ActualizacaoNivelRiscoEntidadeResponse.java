
package pt.baieuropa.baieserv.service.dto.atualizarNivelRiscoEntidade;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "actualizacaoNivelRiscoEntidadeResponse", namespace = "http://actualizacaonivelriscoentidade.consultas")
@Data
public class ActualizacaoNivelRiscoEntidadeResponse {

    @XmlElementRef(name = "return", namespace = "http://actualizacaonivelriscoentidade.consultas", required = false)
    private ObjectActualizacaoNivelRiscoEntidadeOutput _return;


}
