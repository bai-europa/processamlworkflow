
package pt.baieuropa.baieserv.service.dto.atualizarRiscoCliente;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ObjectActualizacaoNivelRiscoClienteInput {

    private String requestId;

    @NotNull(message = "The property [accao] can't be NULL.")
    private String accao;
    @NotNull(message = "The property [autorizacao] can't be NULL.")
    private String autorizacao;
    @NotNull(message = "The property [codigoCampo] can't be NULL.")
    private String codigoCampo;
    @NotNull(message = "The property [numeroBalcao] can't be NULL.")
    private Integer numeroBalcao;
    @NotNull(message = "The property [numeroCliente] can't be NULL.")
    private Integer numeroCliente;
    @NotNull(message = "The property [utilizador] can't be NULL.")
    private String utilizador;
    @NotNull(message = "The property [valorCampo] can't be NULL.")
    private String valorCampo;

}
