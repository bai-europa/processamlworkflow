
package pt.baieuropa.baieserv.service.dto.desativarConta;

import lombok.Data;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "desactivarBloqueioContaResponse", namespace = "http://desactivarBloqueioConta.entidadesclientes")
@Data
public class DesactivarBloqueioContaResponse {

    @XmlElementRef(name = "return", namespace = "http://desactivarBloqueioConta.entidadesclientes", required = false)
    private ObjectOutputDesactivarBloqueioConta _return;

}
