
package pt.baieuropa.baieserv.service.dto.atualizaEstadoFiltering;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dados"
})
@XmlRootElement(name = "atualizaEstadoFiltering", namespace = "http://atualizaestadofiltering.consultas")
@Data
public class AtualizaEstadoFiltering {

    @XmlElementRef(name = "dados", namespace = "http://atualizaestadofiltering.consultas", required = false)
    private ObjectAtualizaEstadoFilteringInput dados;

}
