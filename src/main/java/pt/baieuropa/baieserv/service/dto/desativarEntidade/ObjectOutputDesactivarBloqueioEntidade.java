
package pt.baieuropa.baieserv.service.dto.desativarEntidade;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectOutputDesactivarBloqueioEntidade", namespace = "http://desactivarBloqueioEntidade.entidadesclientes/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
@Data
public class ObjectOutputDesactivarBloqueioEntidade {

    @XmlElement(namespace = "http://desactivarBloqueioEntidade.entidadesclientes/xsd")
    private Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://desactivarBloqueioEntidade.entidadesclientes/xsd", required = false)
    private String mensagemErro;

}
