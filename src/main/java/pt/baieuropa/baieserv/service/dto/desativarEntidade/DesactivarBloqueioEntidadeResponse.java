
package pt.baieuropa.baieserv.service.dto.desativarEntidade;

import lombok.Data;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "desactivarBloqueioEntidadeResponse", namespace = "http://desactivarBloqueioEntidade.entidadesclientes")
@Data
public class DesactivarBloqueioEntidadeResponse {

    @XmlElementRef(name = "return", namespace = "http://desactivarBloqueioEntidade.entidadesclientes", required = false)
    private ObjectOutputDesactivarBloqueioEntidade _return;


}
