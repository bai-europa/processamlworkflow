
package pt.baieuropa.baieserv.service.dto.bloqueioConta;

import lombok.Data;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ObjectOutputCriarBloqueioConta", namespace = "http://criarBloqueioConta.entidadesclientes/xsd", propOrder = {
    "codigoRetorno",
    "mensagemErro"
})
@Data
public class ObjectOutputCriarBloqueioConta {

    @XmlElement(namespace = "http://criarBloqueioConta.entidadesclientes/xsd")
    private Integer codigoRetorno;
    @XmlElementRef(name = "mensagemErro", namespace = "http://criarBloqueioConta.entidadesclientes/xsd", required = false)
    private String mensagemErro;

}
