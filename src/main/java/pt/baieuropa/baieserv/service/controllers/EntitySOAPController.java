package pt.baieuropa.baieserv.service.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.models.account.EntitySOAPRequest;
import pt.baieuropa.baieserv.service.EntitySOAPService;

@Slf4j
@RestController
@RequestMapping({"/aml-entity-soap"})
@RequiredArgsConstructor
@Tag(name = "Entity SOAP", description = "The Entity SOAP API")
public class EntitySOAPController {

    private final EntitySOAPService entityService;
    private final ObjectMapper mapper;

    @PostMapping
    public ResponseEntity<Integer> lockAccount(@Valid @RequestBody EntitySOAPRequest request) throws JsonProcessingException {
        return ResponseEntity.ok(this.entityService.updateEntity(this.mapper.writeValueAsString(request)));
    }
}
