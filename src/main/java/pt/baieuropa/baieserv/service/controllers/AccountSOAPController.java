package pt.baieuropa.baieserv.service.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.service.AccountSOAPService;
import pt.baieuropa.baieserv.service.dto.bloqueioConta.CriarBloqueioConta;

@Slf4j
@Validated
@RestController
@RequestMapping({"/aml-account-soap"})
@RequiredArgsConstructor
@Tag(name = "Account SOAP", description = "The Account SOAP API")
public class AccountSOAPController {

    private final AccountSOAPService accountService;
    private final ObjectMapper mapper;

    @PostMapping
    public ResponseEntity<Integer> lockAccount(@Valid @RequestBody CriarBloqueioConta request) throws JsonProcessingException {
        return ResponseEntity.ok(this.accountService.lockAccount(this.mapper.writeValueAsString(request)));
    }
}
