package pt.baieuropa.baieserv.service;

import pt.baieuropa.baieserv.models.account.EntitySOAPRequest;

public interface EntitySOAPService {

    int lockEntityByParty(String json);
    int updateEntityByParty(String json);
    int unlockEntityByParty(String json);
    int updateEntity(String json);
    int lockEntity(String json);
    int unlockEntity(String json);
    int updateEntityRisk(String json);
    int updateFiltering(String json);
}
