/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.exceptions;

public class MissingParametersException extends Exception {

    public MissingParametersException(String input){
        super(input);
    }
}
