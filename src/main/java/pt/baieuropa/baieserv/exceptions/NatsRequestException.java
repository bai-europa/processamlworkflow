/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.exceptions;

public class NatsRequestException extends Exception {

    public NatsRequestException(String input){
        super(input);
    }
}
