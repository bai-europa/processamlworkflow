/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.exceptions;

public class ConfigurationException extends Exception {

    public ConfigurationException(String message){
        super(message);
    }

}
