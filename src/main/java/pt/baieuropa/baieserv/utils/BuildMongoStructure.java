package pt.baieuropa.baieserv.utils;

import pt.baieuropa.baieserv.repository.entities.MongoStructure;

public class BuildMongoStructure {

    private static final String serviceName = "ProcessAMLWorkflow";

    public static MongoStructure buildMongoStructure(String requestID, String request, String json) {

        return MongoStructure.builder()
                .requestId(requestID)
                .serviceName(serviceName)
                .subject(request)
                .message(json)
                .status("SUCCESS").build();
    }
}
