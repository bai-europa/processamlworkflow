package pt.baieuropa.baieserv.models.account;

import lombok.AllArgsConstructor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EntitySOAPRequest {

    private String partyId;
}
