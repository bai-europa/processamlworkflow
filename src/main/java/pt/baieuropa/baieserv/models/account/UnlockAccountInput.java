package pt.baieuropa.baieserv.models.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.constraints.NotNull;

@JsonPropertyOrder({"user", "accountNumber", "clientNumber", "blockType", "unlockCode", "alertMessage", "authorization", "validateOrApply"})
public class UnlockAccountInput {
    @JsonProperty("user")
    private @NotNull(
            message = "The property [user] can't be NULL."
    ) String user;
    @JsonProperty("accountNumber")
    private @NotNull(
            message = "The property [accountNumber] can't be NULL."
    ) String accountNumber;
    @JsonProperty("clientNumber")
    private @NotNull(
            message = "The property [clientNumber] can't be NULL."
    ) String clientNumber;
    @JsonProperty("blockType")
    private @NotNull(
            message = "The property [blockType] can't be NULL."
    ) String blockType;
    @JsonProperty("unlockCode")
    private @NotNull(
            message = "The property [unlockCode] can't be NULL."
    ) String unlockCode;
    @JsonProperty("alertMessage")
    private @NotNull(
            message = "The property [alertMessage] can't be NULL."
    ) String alertMessage;
    @JsonProperty("authorization")
    private @NotNull(
            message = "The property [authorization] can't be NULL."
    ) String authorization;
    @JsonProperty("validateOrApply")
    private @NotNull(
            message = "The property [validateOrApply] can't be NULL."
    ) String validateOrApply;

    public UnlockAccountInput() {
    }

    public UnlockAccountInput(@NotNull(
            message = "The property [user] can't be NULL."
    ) String user, @NotNull(
            message = "The property [accountNumber] can't be NULL."
    ) String accountNumber, @NotNull(
            message = "The property [clientNumber] can't be NULL."
    ) String clientNumber, @NotNull(
            message = "The property [blockType] can't be NULL."
    ) String blockType, @NotNull(
            message = "The property [unlockCode] can't be NULL."
    ) String unlockCode, @NotNull(
            message = "The property [alertMessage] can't be NULL."
    ) String alertMessage, @NotNull(
            message = "The property [authorization] can't be NULL."
    ) String authorization, @NotNull(
            message = "The property [validateOrApply] can't be NULL."
    ) String validateOrApply) {
        this.user = user;
        this.accountNumber = accountNumber;
        this.clientNumber = clientNumber;
        this.blockType = blockType;
        this.unlockCode = unlockCode;
        this.alertMessage = alertMessage;
        this.authorization = authorization;
        this.validateOrApply = validateOrApply;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getAccountNumber() {
        return this.accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getClientNumber() {
        return this.clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getBlockType() {
        return this.blockType;
    }

    public void setBlockType(String blockType) {
        this.blockType = blockType;
    }

    public String getUnlockCode() {
        return this.unlockCode;
    }

    public void setUnlockCode(String unlockCode) {
        this.unlockCode = unlockCode;
    }

    public String getAlertMessage() {
        return this.alertMessage;
    }

    public void setAlertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }

    public String getAuthorization() {
        return this.authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getValidateOrApply() {
        return this.validateOrApply;
    }

    public void setValidateOrApply(String validateOrApply) {
        this.validateOrApply = validateOrApply;
    }
}
