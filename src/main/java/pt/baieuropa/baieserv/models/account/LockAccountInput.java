package pt.baieuropa.baieserv.models.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.constraints.NotNull;

@JsonPropertyOrder({"user", "accountNumber", "blockType", "blockCode", "alertMessage", "authorization", "validateOrApply"})
public class LockAccountInput {
    @JsonProperty("user")
    private @NotNull(
            message = "The property [user] can't be NULL."
    ) String user;
    @JsonProperty("accountNumber")
    private @NotNull(
            message = "The property [accountNumber] can't be NULL."
    ) String accountNumber;
    @JsonProperty("blockType")
    private @NotNull(
            message = "The property [blockType] can't be NULL."
    ) String blockType;
    @JsonProperty("blockCode")
    private @NotNull(
            message = "The property [blockCode] can't be NULL."
    ) String blockCode;
    @JsonProperty("alertMessage")
    private @NotNull(
            message = "The property [alertMessage] can't be NULL."
    ) String alertMessage;
    @JsonProperty("authorization")
    private @NotNull(
            message = "The property [authorization] can't be NULL."
    ) String authorization;
    @JsonProperty("validateOrApply")
    private @NotNull(
            message = "The property [validateOrApply] can't be NULL."
    ) String validateOrApply;

    public LockAccountInput() {
    }

    public LockAccountInput(@NotNull(
            message = "The property [user] can't be NULL."
    ) String user, @NotNull(
            message = "The property [accountNumber] can't be NULL."
    ) String accountNumber, @NotNull(
            message = "The property [blockType] can't be NULL."
    ) String blockType, @NotNull(
            message = "The property [blockCode] can't be NULL."
    ) String blockCode, @NotNull(
            message = "The property [alertMessage] can't be NULL."
    ) String alertMessage, @NotNull(
            message = "The property [authorization] can't be NULL."
    ) String authorization, @NotNull(
            message = "The property [validateOrApply] can't be NULL."
    ) String validateOrApply) {
        this.user = user;
        this.accountNumber = accountNumber;
        this.blockType = blockType;
        this.blockCode = blockCode;
        this.alertMessage = alertMessage;
        this.authorization = authorization;
        this.validateOrApply = validateOrApply;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getaccountNumber() {
        return this.accountNumber;
    }

    public void setaccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getblockType() {
        return this.blockType;
    }

    public void setblockType(String blockType) {
        this.blockType = blockType;
    }

    public String getblockCode() {
        return this.blockCode;
    }

    public void setblockCode(String blockCode) {
        this.blockCode = blockCode;
    }

    public String getalertMessage() {
        return this.alertMessage;
    }

    public void setalertMessage(String alertMessage) {
        this.alertMessage = alertMessage;
    }

    public String getauthorization() {
        return this.authorization;
    }

    public void setauthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getvalidateOrApply() {
        return this.validateOrApply;
    }

    public void setvalidateOrApply(String validateOrApply) {
        this.validateOrApply = validateOrApply;
    }
}
