package pt.baieuropa.baieserv.models.account;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.constraints.NotNull;

@JsonPropertyOrder({"user", "clientNumber", "action", "authorization", "fieldCode", "branchNumber", "fieldValue"})
public class ClientRiskInput {
    @JsonProperty("user")
    private @NotNull(
            message = "The property [user] can't be NULL."
    ) String user;
    @JsonProperty("clientNumber")
    private @NotNull(
            message = "The property [clientNumber] can't be NULL."
    ) String clientNumber;
    @JsonProperty("action")
    private @NotNull(
            message = "The property [action] can't be NULL."
    ) String action;
    @JsonProperty("authorization")
    private @NotNull(
            message = "The property [authorization] can't be NULL."
    ) String authorization;
    @JsonProperty("fieldCode")
    private @NotNull(
            message = "The property [fieldCode] can't be NULL."
    ) String fieldCode;
    @JsonProperty("branchNumber")
    private @NotNull(
            message = "The property [branchNumber] can't be NULL."
    ) String branchNumber;
    @JsonProperty("fieldValue")
    private @NotNull(
            message = "The property [fieldValue] can't be NULL."
    ) String fieldValue;

    public ClientRiskInput() {
    }

    public ClientRiskInput(@NotNull(
            message = "The property [user] can't be NULL."
    ) String user, @NotNull(
            message = "The property [clientNumber] can't be NULL."
    ) String clientNumber, @NotNull(
            message = "The property [action] can't be NULL."
    ) String action, @NotNull(
            message = "The property [authorization] can't be NULL."
    ) String authorization, @NotNull(
            message = "The property [fieldCode] can't be NULL."
    ) String fieldCode, @NotNull(
            message = "The property [branchNumber] can't be NULL."
    ) String branchNumber, @NotNull(
            message = "The property [fieldValue] can't be NULL."
    ) String fieldValue) {
        this.user = user;
        this.clientNumber = clientNumber;
        this.action = action;
        this.authorization = authorization;
        this.fieldCode = fieldCode;
        this.branchNumber = branchNumber;
        this.fieldValue = fieldValue;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getClientNumber() {
        return this.clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAuthorization() {
        return this.authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getFieldCode() {
        return this.fieldCode;
    }

    public void setFieldCode(String fieldCode) {
        this.fieldCode = fieldCode;
    }

    public String getBranchNumber() {
        return this.branchNumber;
    }

    public void setBranchNumber(String branchNumber) {
        this.branchNumber = branchNumber;
    }

    public String getFieldValue() {
        return this.fieldValue;
    }

    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
}
