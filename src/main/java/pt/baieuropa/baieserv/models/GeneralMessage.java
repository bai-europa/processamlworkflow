package pt.baieuropa.baieserv.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(
        ignoreUnknown = true
)
public class GeneralMessage {
    @JsonProperty("ID")
    private int id;
    @JsonProperty("MessageType")
    private String messageType;
    @JsonProperty("Message")
    private Object message;

    public GeneralMessage(int id, String messageType, Object message) {
        this.id = id;
        this.messageType = messageType;
        this.message = message;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessageType() {
        return this.messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Object getMessage() {
        return this.message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }
}
