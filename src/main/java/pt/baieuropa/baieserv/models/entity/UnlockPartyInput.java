package pt.baieuropa.baieserv.models.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import jakarta.validation.constraints.NotNull;

@JsonPropertyOrder({"partyId", "blockType", "blockCode", "authorization", "validateOrApply", "user"})
public class UnlockPartyInput {
    @JsonProperty("partyId")
    private @NotNull(
            message = "The property [partyId] can't be NULL."
    ) String partyId;
    @JsonProperty("blockType")
    private @NotNull(
            message = "The property [blockType] can't be NULL."
    ) String blockType;
    @JsonProperty("blockCode")
    private @NotNull(
            message = "The property [blockCode] can't be NULL."
    ) String blockCode;
    @JsonProperty("authorization")
    private @NotNull(
            message = "The property [authorization] can't be NULL."
    ) String authorization;
    @JsonProperty("validateOrApply")
    private @NotNull(
            message = "The property [validateOrApply] can't be NULL."
    ) String validateOrApply;
    @JsonProperty("user")
    private @NotNull(
            message = "The property [user] can't be NULL."
    ) String user;

    public UnlockPartyInput() {
    }

    public UnlockPartyInput(@NotNull(
            message = "The property [partyId] can't be NULL."
    ) String partyId, @NotNull(
            message = "The property [blockType] can't be NULL."
    ) String blockType, @NotNull(
            message = "The property [blockCode] can't be NULL."
    ) String blockCode, @NotNull(
            message = "The property [authorization] can't be NULL."
    ) String authorization, @NotNull(
            message = "The property [validateOrApply] can't be NULL."
    ) String validateOrApply, @NotNull(
            message = "The property [user] can't be NULL."
    ) String user) {
        this.partyId = partyId;
        this.blockType = blockType;
        this.blockCode = blockCode;
        this.authorization = authorization;
        this.validateOrApply = validateOrApply;
        this.user = user;
    }

    public String getPartyId() {
        return this.partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getBlockType() {
        return this.blockType;
    }

    public void setBlockType(String blockType) {
        this.blockType = blockType;
    }

    public String getBlockCode() {
        return this.blockCode;
    }

    public void setBlockCode(String blockCode) {
        this.blockCode = blockCode;
    }

    public String getAuthorization() {
        return this.authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getValidateOrApply() {
        return this.validateOrApply;
    }

    public void setValidateOrApply(String validateOrApply) {
        this.validateOrApply = validateOrApply;
    }

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
