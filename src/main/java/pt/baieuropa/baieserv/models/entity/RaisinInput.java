package pt.baieuropa.baieserv.models.entity;

import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {
        "entityName",
        "docId",
        "nif",
        "birthDt",
        "entityNr"
})
public class RaisinInput {

    private String entityName;
    private String docId;
    private String nif;
    private String birthDt;
    private String entityNr;


    public RaisinInput() {
    }

    public RaisinInput(String entityName, String docId, String nif, String birthDt, String entityNr) {
        this.entityName = entityName;
        this.docId = docId;
        this.nif = nif;
        this.birthDt = birthDt;
        this.entityNr = entityNr;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getBirthDt() {
        return birthDt;
    }

    public void setBirthDt(String birthDt) {
        this.birthDt = birthDt;
    }

    public String getEntityNr() {
        return entityNr;
    }

    public void setEntityNr(String entityNr) {
        this.entityNr = entityNr;
    }
}
