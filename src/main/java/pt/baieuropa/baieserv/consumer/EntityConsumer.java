package pt.baieuropa.baieserv.consumer;

import lib.baieuropa.aop.logs.AsyncCorrelationId;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.artemis.jms.client.ActiveMQTextMessage;
import org.json.JSONObject;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import pt.baieuropa.baieserv.service.impl.EntitySOAPImpl;

@Slf4j
@Component
@RequiredArgsConstructor
public class EntityConsumer {

  private final EntitySOAPImpl entityController;

  @AsyncCorrelationId
  @JmsListener(destination = "${mq.entity.create_particular_request}", containerFactory = "defaultFactory")
  public void createEntityParticularRequest(ActiveMQTextMessage message) {

    log.info("EntityConsumer@createEntityParticularRequest - Input - Received message: {}",
        message.getText());
    final var jsonObject = new JSONObject(message.getText());

    if (jsonObject.has("PartyType") && !"5".equals(jsonObject.getString("PartyType"))) {
      entityController.lockEntityByParty(message.getText());
    }
  }

  @AsyncCorrelationId
  @JmsListener(destination = "${mq.entity.enterprise_created}", containerFactory = "defaultFactory")
  public void createEntityEnterpriseRequest(ActiveMQTextMessage message) {

    log.info("EntityConsumer@createEntityEnterpriseRequest - Input - Received message: {}",
        message.getText());
    final var jsonObject = new JSONObject(message.getText());

    if (jsonObject.has("PartyType") && !"5".equals(jsonObject.getString("PartyType"))) {
      entityController.lockEntityByParty(message.getText());
    }
  }

  @AsyncCorrelationId
  @JmsListener(destination = "${mq.entity.particular_updated}", containerFactory = "defaultFactory")
  public void updateEntityParticularRequest(ActiveMQTextMessage message) {

    log.info("EntityConsumer@updateEntityParticularRequest - Input - Received message: {}",
        message.getText());
    entityController.updateEntity(message.getText());
  }

  @AsyncCorrelationId
  @JmsListener(destination = "${mq.entity.enterprise_updated}", containerFactory = "defaultFactory")
  public void updateEntityEnterpriseRequest(ActiveMQTextMessage message) {

    log.info("EntityConsumer@updateEntityEnterpriseRequest - Input - Received message: {}",
        message.getText());
    entityController.updateEntity(message.getText());
  }
}
