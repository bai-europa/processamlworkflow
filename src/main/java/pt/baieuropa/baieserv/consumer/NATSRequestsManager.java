package pt.baieuropa.baieserv.consumer;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import lib.baieuropa.aop.errors.handle.ExceptionHandler;
import lib.baieuropa.aop.logs.AsyncCorrelationId;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.AsyncTaskExecutor;
import pt.baieuropa.baieserv.service.impl.AccountSOAPServiceImpl;
import pt.baieuropa.baieserv.service.impl.AmlSOAPServiceImpl;
import pt.baieuropa.baieserv.service.impl.EntitySOAPImpl;
import pt.baieuropa.nats.Runner;

@Slf4j
@RequiredArgsConstructor
public class NATSRequestsManager implements Runner<Void> {

  private final String topic;
  private final AmlSOAPServiceImpl amlSoapServiceImpl;
  private final AccountSOAPServiceImpl accountController;
  private final EntitySOAPImpl entityController;

  @Autowired
  @Qualifier("applicationTaskExecutor")
  private AsyncTaskExecutor asyncTaskExecutor;

  @Override
  public String getTopico() {
    return topic;
  }

  @Override
  @AsyncCorrelationId
  @ExceptionHandler
  public boolean run(byte[] input) {
    final var correlationId = MDC.get("correlation_id");
    CompletableFuture.runAsync(() -> {
      MDC.put("correlation_id", correlationId);
      String jsonString = new String(input, StandardCharsets.UTF_8);
      JSONObject jsonObject = new JSONObject(jsonString);

      String partyType = "";
      if (jsonObject.has("PartyType")) {
        partyType = jsonObject.getString("PartyType");
      }
      try {
        switch (topic) {
          case "AML:RequestUnlockEntity" -> amlSoapServiceImpl.unlockEntity(jsonString);
          case "LockAccount:Request" -> accountController.lockAccount(jsonString);
          case "UnlockAccount:Request" -> accountController.unlockAccount(jsonString);
          case "UpdateAccountRisk:Request" -> accountController.updateAccountRisk(jsonString);
          case "AML:RequestLockEntity", "LockEntity:Request" -> {
            if (!"5".equals(partyType)) {
              entityController.lockEntity(jsonString);
            }
          }
          case "UnlockEntity:Request" -> entityController.unlockEntity(jsonString);
          case "UpdateEntityRisk:Request" -> entityController.updateEntityRisk(jsonString);
          case "UpdateFiltering:Request" -> entityController.updateFiltering(jsonString);
          case "EnterpriseCreated:Request" -> {
            if (!"5".equals(partyType)) {
              entityController.lockEntityByParty(jsonString);
            }
          }
          case "UpdateEntity:Request" ->
              entityController.updateEntity(jsonString);
          case "CreateAccount:Request" -> accountController.lockAccountByParty(jsonString);
          case "UpdateAccount:Request" -> accountController.updateAccountByParty(jsonString);
        }
      } catch (RuntimeException e) {
        log.error("NATSRequestsManager@run - Output - {}", e.getLocalizedMessage());
      }

    }, asyncTaskExecutor);
    return true;
  }
}
